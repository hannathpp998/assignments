package Java;

import java.util.Scanner;

public class StringPalindrome {

	public static void main(String[] args) {
		 System.out.println("Enter string:");
			Scanner sc=new Scanner(System.in);
			String string,reverse="";
			string=sc.nextLine();
		    char[] name=string.toCharArray();
			for(int i=name.length-1;i>=0;i--) {
	    	    reverse=reverse.concat(String.valueOf(name[i]));
		    }
		    if(string.equalsIgnoreCase(reverse)){
		        System.out.println(string+" is palindrome");
		    }
		    else{
		        System.out.println(string+" is not palindrome");
		    }
			sc.close();

	}

}
