package Java;

import java.util.Scanner;

public class Swapping {

	public static void main(String[] args) {
		System.out.println("Enter value of a and b");
		int a,b;
		Scanner sc = new Scanner(System.in);
		a=sc.nextInt();
		b=sc.nextInt();
		System.out.println("Before swapping values of a and b are: "+a+" "+b);
		a=a+b;
		b=a-b;
		a=a-b;
		System.out.println("After swapping values of a and b are: "+a+" "+b);
		sc.close();

	}

}
